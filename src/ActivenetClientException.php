<?php

namespace Drupal\activenet;

/**
 * Class of ActivenetClientException.
 *
 * @package Drupal\activenet
 */
class ActivenetClientException extends \Exception {

}
